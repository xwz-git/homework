package com.lagou.dao;

import com.lagou.pojo.User;

import java.util.List;

public interface IUserDao {

    //查询所有用户
    public List<User> findAll() throws Exception;


    //根据条件进行用户查询
    public User findByCondition(User user) throws Exception;

    // 添加
    public int save(User user);
    // 更新
    public int updateById(User user);
    // 删除
    public int delateById(User user);


}
