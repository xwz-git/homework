package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {

    @Test
    public void test() throws Exception {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //调用
        User user = new User();
        user.setId(1);
        user.setUsername("张三");
      /*  User user2 = sqlSession.selectOne("user.selectOne", user);

        System.out.println(user2);*/

       /* List<User> users = sqlSession.selectList("user.selectList");
        for (User user1 : users) {
            System.out.println(user1);
        }*/

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

//        List<User> all = userDao.findAll();
//        for (User user1 : all) {
//            System.out.println(user1);
//        }

        // 根据条件Id查询到user
        User condition = new User();
        condition.setId(1);
        User byCondition = userDao.findByCondition(condition);
        System.out.println(byCondition);


    }

    @Test
    public void testUpdate() throws Exception{
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        // 新增用户
        User addUser = new User();
        addUser.setId(10);
        addUser.setUsername("新增用户test");
        System.out.println("----------新增-----------");
        System.out.println(userDao.save(addUser));
        System.out.println(userDao.findByCondition(addUser));

        // 根据id更新user
        System.out.println("----------修改-----------");
        addUser.setUsername("修改update name");
        System.out.println(userDao.updateById(addUser));
        System.out.println(userDao.findByCondition(addUser));


        // 删除用户
        System.out.println("-----------删除----------");
        System.out.println(userDao.delateById(addUser));
        System.out.println(userDao.findByCondition(addUser));

    }



}
