**一、编程题**

学员自定义@Service、@Autowired、@Transactional注解类，完成基于注解的IOC容器（Bean对象创建及依赖注入维护）和声明式事务控制，写到转账工程中，并且可以实现转账成功和转账异常时事务回滚

注意考虑以下情况：

 1）注解有无value属性值【@service（value=""） @Repository（value=""）】 

 2）service层是否实现接口的情况【jdk还是cglib】

**二、作业资料说明：**

1、提供资料：代码工程、验证及讲解视频、SQL脚本。

2、讲解内容包含：题目分析、实现思路、代码讲解。

3、效果视频验证

  1）实现转账成功和转账异常时事务回滚。

  2）展示和讲解自定义@Service、@Autowired、@Transactional注解类。