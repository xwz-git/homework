package com.lagou.edu.anno;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@MyComponent
public @interface MyRepository {
    /**
     * 注解内容值
     * @return
     */
    String value() default "";
}
