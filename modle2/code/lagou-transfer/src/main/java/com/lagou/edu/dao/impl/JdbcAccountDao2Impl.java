package com.lagou.edu.dao.impl;

import com.lagou.edu.anno.MyService;
import com.lagou.edu.dao.AccountDao;
import com.lagou.edu.pojo.Account;

/**
 * @ClassName JdbcAccountDao2Impl
 * @Descriptioin
 * @Author xiewenzhuang
 * @Date 2020/12/15 0015 16:34
 * @Version 1.0
 */
@MyService
public class JdbcAccountDao2Impl implements AccountDao {
    @Override
    public Account queryAccountByCardNo(String cardNo) throws Exception {
        return new Account();
    }

    @Override
    public int updateAccountByCardNo(Account account) throws Exception {
        return 0;
    }
}
