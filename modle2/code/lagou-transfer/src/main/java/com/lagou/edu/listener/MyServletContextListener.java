package com.lagou.edu.listener;

import com.lagou.edu.factory.AnnotationConfigApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {
    public static final String CONFIG_LOCATION_PARAM = "scanPackage";
    public static final String CONTEXT_KEY = "CONTEXT_KEY";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        String scanPackage = servletContext.getInitParameter(CONFIG_LOCATION_PARAM); // com.lagou.edu
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(scanPackage);
        // context.getBean()
        servletContext.setAttribute(CONTEXT_KEY,context);
        System.out.println(servletContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        System.out.println("销毁容器");
    }
}
