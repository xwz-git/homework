package com.lagou.edu.anno;

import java.lang.annotation.*;

/**
 * 组件注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyComponent {

    /**
     * 注解内容值
     * @return
     */
    String value() default "";
}
