package server;

import java.util.HashMap;
import java.util.Map;

public class Context {
    private Map<String, Wrapper> wrapperMap = new HashMap<>();

    public Map<String, Wrapper> getWrapperMap() {
        return this.wrapperMap;
    }

    public void setWrapperMap(final Map<String, Wrapper> wrapperMap) {
        this.wrapperMap = wrapperMap;
    }

    public HttpServlet getServlet(String path) {
        Wrapper wrapper = wrapperMap.get(path);
        if(wrapper == null){
            return null;
        }
        return wrapper.getHttpServlet();
    }
}
