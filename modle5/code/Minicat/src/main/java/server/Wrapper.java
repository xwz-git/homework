package server;

import java.net.URL;
import java.net.URLClassLoader;

public class Wrapper {
    private HttpServlet httpServlet;

    public Wrapper(String base, String contextPath, String classPathName) {
        try {
            // 根据绝对路径加载每个demo下的LagouServlet
            URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[]{new URL("file://"+base+"/"+contextPath+"/WEB-INF/classes/")});
            Class<HttpServlet> aClass = (Class<HttpServlet>)urlClassLoader.loadClass(classPathName);
            this.httpServlet = aClass.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HttpServlet getHttpServlet() {
        return this.httpServlet;
    }

    public void setHttpServlet(final HttpServlet httpServlet) {
        this.httpServlet = httpServlet;
    }


    public static void main(String[] args) {
        try {
            URL[] urls = {new URL("file:///Volumes/MAC_DATA/ideaProject/homework2/modle5/code/webapps/demo1/WEB-INF/classes/")};
            URLClassLoader urlClassLoader = URLClassLoader.newInstance(urls);
            Class aClass = urlClassLoader.loadClass("com.lagou.LagouServlet");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
