package server;

import java.util.HashMap;
import java.util.Map;

public class Mapper {
    Map<String, Host> hostMap = new HashMap<>();

    public Map<String, Host> getHostMap() {
        return this.hostMap;
    }

    public void setHostMap(final Map<String, Host> hostMap) {
        this.hostMap = hostMap;
    }

    // localhost /demo1/lagou
    public HttpServlet getServlet(String hostName,String url) {
        Host host = hostMap.get(hostName);
        if(host == null){
            return null;
        }
        return host.getServlet(url);
    }
}
