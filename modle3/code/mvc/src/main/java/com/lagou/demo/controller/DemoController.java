package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@LagouController
@LagouRequestMapping("/demo")
@Security({"zhangsan","wangwu"})
public class DemoController {


    @LagouAutowired
    private IDemoService demoService;


    /**
     * URL: /demo/query?name=lisi
     * @param request
     * @param response
     * @param name
     * @return
     */
    @LagouRequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response,String name) {
        return demoService.get(name);
    }

    /**
     * URL： /demo/handle01?username=zhangsan
     * URL： /demo/handle01?username=lisi
     * URL： /demo/handle01?username=wangwu
     * @param username
     * @return
     */
    @LagouRequestMapping("/handle01")
    public String handle01(String username,HttpServletRequest request, HttpServletResponse response){
        System.out.println("授权通过:"+username);
        return username;
    }
}
