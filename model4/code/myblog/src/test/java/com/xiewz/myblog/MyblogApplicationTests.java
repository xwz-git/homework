package com.xiewz.myblog;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiewz.mapper.ArticleMapper;
import com.xiewz.pojo.Article;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MyblogApplicationTests {

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    void contextLoads() {
        List<Article> articles = articleMapper.selectList(new LambdaQueryWrapper<>());
        System.out.println(articles);
    }

    @Test
    public void testPage(){

        Page<Article> articlePage = new Page<>();
        articlePage.setCurrent(1);
        articlePage.setSize(3);
        IPage<Article> articleIPage = articleMapper.selectPage(articlePage, new LambdaQueryWrapper<>());
        System.out.println(articleIPage.getCurrent());
        System.out.println(articleIPage.getSize());
        System.out.println(articleIPage.getTotal());
        for (Article record : articleIPage.getRecords()) {
            System.out.println(record);
        }
    }

    @Test
    public void testById(){
        Article article = articleMapper.selectArticleById(1);
        System.out.println(article);
    }

}
