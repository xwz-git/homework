package com.xiewz.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xiewz.pojo.Article;

public interface ArticleService {

    /**
     * 分页查询
     * @param condition 条件
     * @param pageNo 当前页
     * @param pageSize 每页长度
     * @return
     */
    IPage<Article> findPage(Article condition, Integer pageNo,Integer pageSize);
}
