package com.xiewz.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiewz.pojo.Article;

public interface ArticleMapper extends BaseMapper<Article> {
    Article selectArticleById(Integer id);
}
