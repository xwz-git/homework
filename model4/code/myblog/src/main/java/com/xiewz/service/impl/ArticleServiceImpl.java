package com.xiewz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiewz.mapper.ArticleMapper;
import com.xiewz.pojo.Article;
import com.xiewz.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;


    @Override
    public IPage<Article> findPage(Article condition, Integer pageNo, Integer pageSize) {
        // 1. 组装分页参数
        Page<Article> articlePage = new Page<>();
        articlePage.setCurrent(pageNo == null ? 1 : pageNo);
        articlePage.setSize(pageSize == null ? 3 : pageSize);
        // 2. 组装查询参数
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        if (condition != null) {
            queryWrapper.eq(condition.getId() != null, Article::getId, condition.getId())
                    .eq(StringUtils.isNotEmpty(condition.getTitle()), Article::getTitle, condition.getTitle())
                    .eq(StringUtils.isNotEmpty(condition.getCategories()), Article::getCategories, condition.getCategories())
                    .eq(StringUtils.isNotEmpty(condition.getTags()), Article::getTags, condition.getTags())
            ;
        }

        // 3. 查询结果返回
        IPage<Article> resultPage = articleMapper.selectPage(articlePage, queryWrapper);
        return resultPage;
    }
}
