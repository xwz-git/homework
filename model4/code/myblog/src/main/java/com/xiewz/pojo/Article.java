package com.xiewz.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("t_article")
public class Article {


    @TableId
    private Integer id;
    /**
     * ⽂文章标题
     */
    private String title;
    /**
     * 文文章具体内容
     */
    private String content;
    /**
     * 发表时间
     */
    private Date created;
    /**
     * 修改时间
     */
    private Date modified;
    /**
     * 文文章分类
     */
    private String categories;
    /**
     * 文文章标签
     */
    private String tags;
    /**
     * 是否允许评论
     */
    @TableField("allow_comment")
    private Boolean allowComment;
    /**
     * 文文章缩略略图
     */
    private String thumbnail;

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public Date getModified() {
        return this.modified;
    }

    public void setModified(final Date modified) {
        this.modified = modified;
    }

    public String getCategories() {
        return this.categories;
    }

    public void setCategories(final String categories) {
        this.categories = categories;
    }

    public String getTags() {
        return this.tags;
    }

    public void setTags(final String tags) {
        this.tags = tags;
    }

    public Boolean getAllowComment() {
        return this.allowComment;
    }

    public void setAllowComment(final Boolean allowComment) {
        this.allowComment = allowComment;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(final String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", created=" + created +
                ", modified=" + modified +
                ", categories='" + categories + '\'' +
                ", tags='" + tags + '\'' +
                ", allowComment=" + allowComment +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
