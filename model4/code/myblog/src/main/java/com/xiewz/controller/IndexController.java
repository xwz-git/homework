package com.xiewz.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xiewz.pojo.Article;
import com.xiewz.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class IndexController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/")
    public String home(Model model){
        return index(model,1,3);
    }

    @RequestMapping("/index")
    public String index(Model model,Integer currentNo,Integer pageSize){

        IPage<Article> page = articleService.findPage(new Article(), currentNo, pageSize);
        model.addAttribute("articles",page.getRecords());
        model.addAttribute("current",page.getCurrent());
        model.addAttribute("pages",page.getPages());
        model.addAttribute("pageSize",page.getSize());
        model.addAttribute("total",page.getTotal());

        return "client/index";
    }
}
