SQL语句
```sql
create DATABASE blog_system
DROP TABLE IF EXISTS t_article;
CREATE TABLE t_article (
id int(11) NOT NULL AUTO_INCREMENT,
title varchar(50) NOT NULL COMMENT '文文章标题',
content longtext COMMENT '文文章具体内容',
created date NOT NULL COMMENT '发表时间',
modified date DEFAULT NULL COMMENT '修改时间',
categories varchar(200) DEFAULT '默认分类' COMMENT '文文章分类',
tags varchar(200) DEFAULT NULL COMMENT '文文章标签',
allow_comment tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否允许评论', 
thumbnail varchar(200) DEFAULT NULL COMMENT '文文章缩略略图',
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO t_article VALUES ('1', '2019新版Java学习路路线图','Java学习路路线图具体内容 具体内容具体内容具体内容具体内容具体内容具体内容','2019-10-10', null, '默认分类', '‘2019,Java,学习路路线图', '1', null);
INSERT INTO t_article VALUES ('2', '2019新版Python学习线路路图','据悉，Python已经入入驻 ⼩小学生生教材，未来不不学Python不不仅知识会脱节，可能与⼩小朋友都没有了了共同话题~~所以，从今天起不不要再 找借口口，不不要再说想学Python却没有资源，赶快行行行动起来，Python等你来探索' ,'2019-10-10', null, '默认分类', '‘2019,Java,学习路路线图', '1', null);
INSERT INTO t_article VALUES ('3', 'JDK 8——Lambda表达式介绍',' Lambda表达式是JDK 8中一一个重要的新特性，它使用用一一个清晰简洁的表达式来表达一一个接口口，同时Lambda表达式也简化了了对集合 以及数组数据的遍历、过滤和提取等操作。下面面，本篇文文章就对Lambda表达式进行行行简要介绍，并进行行行演示 说明' ,'2019-10-10', null, '默认分类', '‘2019,Java,学习路路线图', '1', null);
INSERT INTO t_article VALUES ('4', '函数式接口口','虽然Lambda表达式可以实现匿匿名内部类的 功能，但在使用用时却有一一个局限，即接口口中有且只有一一个抽象方方法时才能使用用Lamdba表达式代替匿匿名内部 类。这是因为Lamdba表达式是基于函数式接口口实现的，所谓函数式接口口是指有且仅有一一个抽象方方法的接 口口，Lambda表达式就是Java中函数式编程的体现，只有确保接口口中有且仅有一一个抽象方方法，Lambda表达 式才能顺利利地推导出所实现的这个接口口中的方方法' ,'2019-10-10', null, '默认分类', '‘2019,Java,学习路路线图', '1', null); 
INSERT INTO t_article VALUES ('5', '虚拟化容器器技术——Docker运行机制介绍','Docker是一一 个开源的应用用容器器引擎，它基于go语⾔言开发，并遵从Apache2.0开源协议。使用用Docker可以让开发者封装 他们的应用用以及依赖包到一一个可移植的容器器中，然后发布到任意的Linux机器器上，也可以实现虚拟化。 Docker容器器完全使用用沙箱机制，相互之间不不会有任何接口口，这保证了了容器器之间的安全性' ,'2019-10- 10', null, '默认分类', '‘2019,Java,学习路路线图', '1', null);

```